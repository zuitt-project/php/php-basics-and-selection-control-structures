<?php

function getLetterGrade ($grade) {
	if($grade <= 98 && >=100) {
		return 'is equivalent to A+';
	}
	else if($grade <=95 && >= 97) {
		return 'is equivalent to A';
	}
	else if($grade <=92 && >=94) {
		return 'is equivalent to A-';
	}
	else if($grade <=89 && >=91) {
		return 'is equivalent to B+'
	}
	else if($grade <=86 && >=88) {
		return 'is equivalent to B'
	}
	else if($grade <=83 && >=85) {
		return 'is equivalent to B-'
	}
	else if($grade <=80 && >=82) {
		return 'is equivalent to C+'
	}
	else if($grade <=77 && >=79) {
		return 'is equivalent to C'
	}
	else if($grade <=75 && >=76) {
		return 'is equivalent to C-'
	}
	else {
		return 'is equivalent to D'
	}
}

?>